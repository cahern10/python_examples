def bubble_sort(list):
    #swap the elements to arrange in order
    for iter_num in range(len(list) - 1, 0, -1):
        for x in range(iter_num):
            if list[x] > list[x + 1]:
                tmp = list[x]
                list[x] = list[x + 1]
                list[x + 1] = tmp

list = [19,2,31,45,6,11,121,27]
bubble_sort(list)
print(list)