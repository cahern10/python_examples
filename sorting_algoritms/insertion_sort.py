def insertion_sort(input_list):
    for x in range(1, len(input_list)):
        tmp = x - 1
        next_element = input_list[x]

        while(input_list[tmp] > next_element and tmp >= 0):
            input_list[tmp + 1] = input_list[tmp]
            tmp -= 1
        input_list[tmp + 1] = next_element

list = [19,2,31,45,30,11,121,27]
insertion_sort(list)
print(list)