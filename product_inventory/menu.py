from inventory import *

class menu:
    def __init__(self):
        self.inventory = inventory()
    
    def add_products(self):
        #receives input from the user to create a new product
        print("got in")
        id = int(raw_input('Enter product id: '))
        product_name = str(raw_input('Enter product name: '))
        product_quantity = int(raw_input('Enter product quantity: '))

        self.inventory.add_products(id, product_name, product_quantity)
        print('Successfully added')

    def add_existing_product(self):
        #Ask the user for a product name and a product quantity
        #to increate the amount of the given product if it exists in stock
        try:
            product_name = raw_input('Enter the product name: ')
            product_quantity = int(raw_input('Enter quantity you wish to add'))
            self.inventory.add_existing_product(product_name, product_quantity)
        except Exception as e:
            print('Enter product name and product quantity')

    def check_product_quantity(self):
        #received a product name and returns product quantity of found
        try:
            product_name = raw_input('Enter a product name: ')
            self.inventory.product_quantity(product_name)
        except Exception as e:
            print('please make sure to enter product name'.capitalize())

    def change_product_name(self):
        #changes product name by looking for all product name and replacing it with new name
        try:
            old_name = raw_input('Enter old product name: ')
            new_name = raw_input('Enter new product name: ')
            self.inventory.modify_product_name(old_name, new_name)
        except Exception as e:
            print('please make sure to enter old name and the new name you want'.capitalize())

    def send_out_some_product(self):
        #deduct some products from the product quantity as demanded by user
        try:
            product_name = raw_input('Enter product name: ')
            product_quantity = int(raw_input('Enter product quantity: '))
            self.inventory.take_product(product_name, product_quantity)
        except Exception as e:
            print('please make sure to enter product name and product quantity'.capitalize())

    def show_all_products(self):
        #display all products to the screen
        self.inventory.list_all_products()

    def display(self):
        print('''
			Welcome to Inventory Manager
			============================
			          Chioces 
			          -------
			     Enter    Function
			     -----    --------  
			     1 -------- Exit
			     2 -------- Add new product
			     3 -------- Check product quantity
			     4 -------- Change product name
			     6 -------- Get some products
			     7 -------- See all products 
			     8 -------- Increase existing product quantity                
			''')

    def run(self):
        #ask user for a user choice and calls a particular method to process user demands
        self.display()

        while True:
            try:
                choice = int(input('Enter your choice: '))
                if choice == 1:
                    exit()
                elif choice == 2:
                    self.add_products()
                elif choice == 3:
                    self.check_product_quantity()
                elif choice == 4:
                    self.change_product_name()
                elif choice == 6:
                    self.send_out_some_product()
                elif choice == 7:
                    self.show_all_products()
                elif choice == 8:
                    self.add_existing_product()
            except Exception as e:
                print(e)

if __name__ == '__main__':
    menu().run()