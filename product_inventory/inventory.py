from product import *

class inventory:
    def __init__(self):
        self.products = []

    def add_products(self, id, name, quantity):
        #add a new product to the products list
        self.products.append(product(id, name, quantity))
        return self.products
    
    def product_quantity(self, product_name):
        #display the product quantity by looking for
        #the product name in the list of products
        for product in self.products:
            if product.name == product_name:
                print(product.quantity)
            else:
                print("That product does not exist")
    
    def modify_product_name(self, old_name, new_name):
        #change the product name by looking for all the product names
        #and replacing it with the new name
        for product in self.products:
            if product.name == old_name:
                product.name = new_name
                print("Name successfully changed")
            else:
                print("That product does not exist")

    def take_product(self, product_name, quantity):
        #look for a given product name and deduct a particular
        #quantity of that product as demanded by the user
        for product in self.products:
            if product.name == product_name:
                if product.quantity <= 0:
                    print ('{} product is finish'.format(product.name.capitalize()))
                elif quantity > product.quantity:
                    print('Your demand is more than available products.Available Product {}'.format(product.quantity))
                else:
                    product.quantity -= quantity
                    print('product successfully taken new quantity is {}'.format(product.quantity))
            else:
                print("That product does not exist")

    def add_existing_product(self, product_name, product_quantity):
        #increase the amount of existing of a particular product in stock
        #by seraching for the product with a given name and adding
        #the desired quantity to it
        for product in self.products:
            if product.name == product_name:
                product.quantity += product_quantity
                print("Added successfully")
            else:
                print('Product [{}] does not exist.Enter [2] to add new product or [7] to view available products'.format(product_name))

    def list_all_products(self):
        #create a new list for any products whose value is great than 0
        new_products = list()
        for product in self.products:
            if product.quantity > 0:
                new_products.append(product)
            else:
                print('{} is finish.'.format(product.name))
        total_products = len(new_products)
        print('''
			Total products is {}
			=================
			'''.format(total_products))

        for product in new_products:
            print(product.name, ' ---> {}'.format(product.quantity))