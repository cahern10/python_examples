# Factorial

Write a program which can compute the factorial of a given numbers. <br />
The results should be printed in a comma-separated sequence on a single line.<br />
Suppose the following input is supplied to the program:<br />
8<br />
Then, the output should be:<br />
40320<br />