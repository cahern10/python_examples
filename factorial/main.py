def factorial (num):
    tmp = num
    for x in range (num-1, 1, -1):
        tmp *= x
    return tmp

def factorial_recursive(num):
    if(num == 1):
        return 1
    else:
        return factorial_recursive(num - 1) * num

print(factorial(6))
print(factorial_recursive(6))