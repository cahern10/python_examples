import threading
import time
import random

number = 1

class my_thread(threading.Thread):
    def run(self):
        global number
        print('This is thread ' + str(number) + ' speaking.')
        n = number
        number += 1
        for x in range(20):
            print('Thread', n, 'counts', x)
            time.sleep(0.1 * random.randint(1, 10))

for x in range(10):
    my_thread().start()