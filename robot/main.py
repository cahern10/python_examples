import math

class robot(object):
    def __init__(self):
        self.raw_data = list()
        self.solution = {"VERTICAL": 0, "HORIZONTAL": 0}
        self.options = {
            "UP" : lambda x: (self.solution["VERTICAL"] + x, "VERTICAL"), 
            "DOWN": lambda x: (self.solution["VERTICAL"] - x, "VERTICAL"),
            "LEFT": lambda x: (self.solution["HORIZONTAL"] - x, "HORIZONTAL"),
            "RIGHT": lambda x: (self.solution["HORIZONTAL"] + x, "HORIZONTAL"),
        }
    def __get_values_from_user(self):
        while True:
            tmp = raw_input()
            if(tmp):
                self.raw_data.append(tmp.split(" "))
            else:
                break
                
    def __parse_data(self):
        for x in range (len(self.raw_data)):
            value, direction = self.options.get(self.raw_data[x][0].upper())(int(self.raw_data[x][1]))
            self.solution[direction] = value

    def get_traveled_distance(self):
        self.__get_values_from_user()
        self.__parse_data()
        tmp = int(math.sqrt( ((self.solution["HORIZONTAL"] - 0)**2) + ((self.solution["VERTICAL"] - 0) ** 2) ) )
        print("Distance Traveled: " + str(tmp))

test = robot()
test.get_traveled_distance()