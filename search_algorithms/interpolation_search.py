def inter_search(values, x):
    min = 0
    max = len(values) - 1
    while min <= max and x >= values[min] and x <= values[max]:
        #find the mid point
        mid = min + int(((float(max - min)/( values[max] - values[min])) * ( x - values[min])))
        #compare values at midpoint
        if values[mid] == x:
            return "Found "+str(x)+" at index "+str(mid)
        
        if values[mid] < x:
            min = mid + 1
    return "Searched element not in the list"

l = [2, 6, 11, 19, 27, 31, 45, 121]
print(inter_search(l, 11))
print(inter_search(l, 121))
print(inter_search(l, 32))