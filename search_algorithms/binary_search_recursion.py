def binary_search(list, min, max, val):
    if(max < min): 
        return None
    else:
        mid_val = min + (max - min)

        if list[mid_val] > val:
            return binary_search(list, min, mid_val - 1, val)
        elif list[mid_val] < val:
            return binary_search(list, mid_val + 1, max, val)
        else:
            return mid_val

list = [8, 11, 24, 56, 88, 131]
print(binary_search(list, 0, 5, 24))
print(binary_search(list, 0, 5, 51))
print(binary_search(list, 0, 5, 88))